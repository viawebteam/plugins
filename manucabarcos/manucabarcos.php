<?php
/**
 * Plugin Name: manucabarcos
 * Plugin URI: http://www.viaweb.net.ar
 * Description: manucabarcos
 * Version: 1.0
 * Author: Manuel Putallaz
 * Author URI: http://www.viaweb.net.ar
 */

/* Cabarcos Motores - 17-06-2020 - Manuel Putallaz */
/* Redirige al listado mayorista cuando un usuario se loguea. */
function listado_mayorista_login_redirect($redirect, $user) {
    $redirect_page_id = url_to_postid($redirect);
    $logged_page_id = wc_get_page_id('checkout');

    print_r('TEST: RedirPage: ' . $redirect_page_id . ", loggedPage: " .  $logged_page_id);
    
    if($redirect_page_id == $logged_page_id) {
        return wc_get_page_permalink('listado-mayorista');
    }

    return $redirect;
}

add_filter('woocommerce_login_redirect','listado_mayorista_login_redirect', 10, 2);


/* Cabarcos Motores - 22-06-2020 - Manuel Putallaz */
/* Ocultamos la categoría Mayorista de todas las páginas, menos de listado-mayorista. */

function prefix_custom_pre_get_posts_query($q) {
	if(is_shop() || !is_page('listado-mayorista')) { //No se mostrarán en estas páginas
	    $tax_query = (array) $q->get('tax_query');
	    
	    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field'    => 'slug',
           'terms'    => array('mayorista'), // Categorías que no mostraremos
           'operator' => 'NOT IN'
	    );
	
	    $q->set('tax_query', $tax_query);
	}
}

add_action( 'woocommerce_product_query', 'prefix_custom_pre_get_posts_query' );